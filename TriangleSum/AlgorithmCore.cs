﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleSum
{
    public class AlgorithmCore
    {
        private List<int> elementsContainer = new List<int>();
        public List<int> FindElements(int[,] matrix)
        {
            elementsContainer.Add(matrix[0, 0]);
            for (int i = 0; i < (matrix.GetLength(0)) -1; i++)
            {
                int y = i;
                int x = y + 1;
                elementsContainer.Add(matrix[x, y]);
            }
            return elementsContainer;
        }
    }
}
