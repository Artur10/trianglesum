﻿using System;
using System.Linq;

namespace TriangleSum
{
    class Program
    {
        static void Main(string[] args)
        {
            AlgorithmCore algorithm = new AlgorithmCore();
            var matrix = Utils.GenerateDefaultMatrix();

            var selectedElements = algorithm.FindElements(matrix);
            int finalresult = selectedElements.Sum();

            Utils.DrawMatrix(matrix, selectedElements);

            Console.WriteLine("Result is: " + finalresult);
            Console.ReadLine();
        }
    }
}
